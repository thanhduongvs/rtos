# RIOT  

This site [RIOT Pages](https://www.riot-os.org/).  

## Đổi công cụ nạp code.  
1. Mặc định khi nap code gõ lệnh `make flash BOARD = stm32f429i-disc1` thì build bằng OpenOCD. Bây giờ chuyển thành Jlink thì như thế nào.  
2. Vào trong thư mục `board/stm32f429i-disc1/Makefile.include`. Thay thế 2 dòng:
```
PORT_LINUX ?= /dev/ttyUSB0
export DEBUG_ADAPTER ?= stlink
#include $(RIOTMAKE)/tools/openocd.inc.mk
```  
bằng  
```
PORT_LINUX ?= /dev/ttyACM0
export DEBUG_ADAPTER ?= jlink
#include $(RIOTMAKE)/tools/openocd.inc.mk
JLINK_DEVICE := stm32f429zi
```  

## Show command line help trong shell  
1. Vào thư mục `/sys/shell/shell.c`.  
2. Trong hàm `static void print_help(const shell_command_t *command_list)`. Thêm đoạn code:  
```
printf("%-20s %s\n", "help", "to see list of all commands");
printf("%-20s %s\n", "/exit", "to exit terminal");
```
Sau khi thêm như sau:  
```
printf("%-20s %s\n", "Command", "Description");
puts("---------------------------------------");
printf("%-20s %s\n", "help", "to see list of all commands");
printf("%-20s %s\n", "/exit", "to exit terminal");
```  
4. Trong hàm `static void handle_input_line(const shell_command_t *command_list, char *line)`. Thêm đoạn code:  
```
printf("shell: command not found: %s\n", argv[0]);
print_help(command_list);
```  
Sau khi thêm như sau:  
```
shell_command_handler_t handler = find_handler(command_list, argv[0]);
    if (handler != NULL) {
        handler(argc, argv);
    }
    else {
        if (strcmp("help", argv[0]) == 0) {
            print_help(command_list);
        }
        else {
            printf("shell: command not found: %s\n", argv[0]);
            print_help(command_list);
        }
    }
```